package com.wynk.youtube.playlist.finder.healthcheck;

import org.springframework.stereotype.Component;

import com.codahale.metrics.health.HealthCheck;

/**
 * This class is used to check the project is runing properly or not.
 * @author root
 *
 */
@Component
public class CoreHealthCheck extends HealthCheck {

	@Override
	protected Result check() throws Exception {
		return Result.healthy();
	}

}
