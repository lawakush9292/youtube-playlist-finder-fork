package com.wynk.youtube.playlist.finder.exception;

import javax.ws.rs.core.Response.Status;

public class UnAuthorizedException extends CustomException {

	private static final long serialVersionUID = 1L;

	public UnAuthorizedException() {
		super(Status.UNAUTHORIZED.getStatusCode(), "UnAuthorized.");
	}

	public UnAuthorizedException(final String message) {
		super(Status.UNAUTHORIZED.getStatusCode(), message);
	}
	
	public UnAuthorizedException(final String message,final  Throwable cause) {
		super(Status.UNAUTHORIZED.getStatusCode(), message,cause);
	}

}
