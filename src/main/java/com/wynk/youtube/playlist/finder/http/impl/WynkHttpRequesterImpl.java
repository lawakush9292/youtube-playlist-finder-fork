package com.wynk.youtube.playlist.finder.http.impl;

import java.util.Arrays;

import javax.ws.rs.ServerErrorException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpStatusCodeException;

import com.wynk.youtube.playlist.finder.exception.CustomException;
import com.wynk.youtube.playlist.finder.http.BaseRequester;
import com.wynk.youtube.playlist.finder.http.WynkHttpRequester;
import com.wynk.youtube.playlist.finder.model.PlayListPayLoad;
import com.wynk.youtube.playlist.finder.util.HeaderParam;

@Repository
public class WynkHttpRequesterImpl implements WynkHttpRequester {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(WynkHttpRequesterImpl.class);
	private final String rootUrl;

	private final BaseRequester baseHttpRequester;

	@Autowired
	public WynkHttpRequesterImpl(
			@Value("${wynk.event.url}") final String rootUrl,
			final BaseRequesterFactory httpRequesterFactory) {
		this.rootUrl = rootUrl;
		this.baseHttpRequester = httpRequesterFactory.createBaseHttpReqester(
				"youtube_api.xml", rootUrl);
	}

	/**
	 * Create An Event On Wynk.
	 */

	@Override
	public void createWynkEvent(final PlayListPayLoad playListPayLoad) {
		final HttpHeaders headers = baseHttpRequester.getBasicHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setCacheControl(HeaderParam.CACHE_CONTROL);

		final HttpEntity<PlayListPayLoad> entity = new HttpEntity<>(
				playListPayLoad, headers);
		try {
			final ResponseEntity<String> response = baseHttpRequester
					.getRestTemplate().exchange(rootUrl, HttpMethod.POST,
							entity, String.class);
			if (response.getStatusCode() == HttpStatus.OK) {
				LOGGER.debug(" Success : Status= {} ", response.getStatusCode());
			} else {
				LOGGER.error("Error while creating event  for video name ={} ",
						playListPayLoad.getName());
				throw new ServerErrorException(response.getStatusCode().value());
			}
		} catch (HttpStatusCodeException e) {
			if (e.getStatusCode() == HttpStatus.BAD_REQUEST) {
				throw new CustomException(HttpStatus.BAD_REQUEST.value(),
						"Invalid Data ");
			} else if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
				throw new CustomException(HttpStatus.NOT_FOUND.value(),
						"Event Url Not Exists ");
			} else if (e.getStatusCode() == HttpStatus.FORBIDDEN) {
				throw new CustomException(HttpStatus.NOT_FOUND.value(),
						"ApiKey Not Valid ");
			}
		} catch (Exception e) {
			throw new ServerErrorException(
					HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
	}

}
