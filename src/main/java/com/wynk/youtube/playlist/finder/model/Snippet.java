package com.wynk.youtube.playlist.finder.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Snippet {

	private String title;

	@SuppressWarnings("unused")
	private Snippet() {
		this(null);
	}

	public Snippet(final String title) {
		this.title = title;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

}
