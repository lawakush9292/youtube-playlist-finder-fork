package com.wynk.youtube.playlist.finder.resource.impl;

import java.util.Optional;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wynk.youtube.playlist.finder.model.PlayList;
import com.wynk.youtube.playlist.finder.resource.PlayListResource;
import com.wynk.youtube.playlist.finder.service.PlayListService;

@Component
public class PlayListResourceImpl implements PlayListResource {

	private final PlayListService playListService;

	@Autowired
	public PlayListResourceImpl(final PlayListService playListService) {
		this.playListService = playListService;
	}

	@Override
	public Response getPlayListItems(final String playList) {
		Optional<PlayList> playlist = playListService.getPlayListItems(playList);
		if (playlist.isPresent()) {
			return Response.ok(playlist.get()).build();
		}
		return Response.noContent().build();
	}

}
