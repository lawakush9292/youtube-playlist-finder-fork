package com.wynk.youtube.playlist.finder.util;

public interface Constants {

	String WATCH_URL = "https://www.youtube.com/watch?v=";
	String PART = "part=snippet,contentDetails";
	String PLAYLIST_ID = "&playlistId=";
	String LIST="&list=";
	String KEY = "&key=";
	
	
	/**
	 * URL Related To Youtube PlayList
	 */
	
	String PLAYLIST_ITEMS_URL="get.playlist.items";
}
