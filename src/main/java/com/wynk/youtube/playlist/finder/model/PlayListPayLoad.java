package com.wynk.youtube.playlist.finder.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wynk.youtube.playlist.finder.util.DateUtil;

@JsonInclude(Include.NON_NULL)
public class PlayListPayLoad {

	private final String name;
	private final String createdAt;
	private final String steamUrl;

	@SuppressWarnings("unused")
	private PlayListPayLoad() {
		this(null, null, null);
	}

	public PlayListPayLoad(final String name, final String createdAt, final String streamRootUrl) {
		this.name = name;
		this.createdAt = createdAt;
		this.steamUrl = streamRootUrl;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("createdAt")
	public String getCreatedAt() {
		return createdAt;
	}

	@JsonProperty("streamUrl")
	public String getSteamUrl() {
		return steamUrl;
	}

	public static PlayListPayLoad createWithItems(final Item item, final String streamRootUrl) {
		return new PlayListPayLoad(item.getSnippet().getTitle(),
				DateUtil.getDateString(item.getContentDetail().getCreateDate()).toString(), streamRootUrl);
	}

}
