package com.wynk.youtube.playlist.finder.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ContentDetail {

	private String videoId;
	
	//google api reutrns date in string format so we it is created typoe string.
	private String createDate;

	@SuppressWarnings("unused")
	private ContentDetail() {
		this(null, null);
	}

	public ContentDetail(final String videoId, final String createDate) {
		this.videoId = videoId;
		this.createDate = createDate;
	}

	@JsonProperty("videoId")
	public String getVideoId() {
		return videoId;
	}

	@JsonProperty("videoPublishedAt")
	public String getCreateDate() {
		return createDate;
	}

}
