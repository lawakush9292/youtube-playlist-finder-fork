package com.wynk.youtube.playlist.finder.application;

import io.dropwizard.Application;
import io.dropwizard.lifecycle.Managed;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.google.common.collect.Lists;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.wynk.youtube.playlist.finder.healthcheck.CoreHealthCheck;
import com.wynk.youtube.playlist.finder.provider.CustomExceptionMapper;
import com.wynk.youtube.playlist.finder.resource.PlayListResource;
import com.wynk.youtube.playlist.finder.resource.impl.PlayListResourceImpl;

public class CoreApplication extends Application<CoreServiceConfiguration> implements Managed{

	private ClassPathXmlApplicationContext classPathXmlApplicationContext;

	public static void main(final String[] args) throws Exception {
		new CoreApplication().run(Lists.newArrayList("server", "application.yaml").
				toArray(new String[2]));
	}

	@Override
	public void run(final CoreServiceConfiguration configuration, 
			final Environment environment) throws Exception {
		classPathXmlApplicationContext = new ClassPathXmlApplicationContext(
				"spring/application-config.xml");
		
		final CoreHealthCheck coreHealthCheck = classPathXmlApplicationContext.getBean(CoreHealthCheck.class);
		
		final PlayListResource productResource = classPathXmlApplicationContext.
				getBean(PlayListResourceImpl.class);
		
		final CustomExceptionMapper cem = classPathXmlApplicationContext.
				getBean(CustomExceptionMapper.class);
		
		environment.healthChecks().register("YouTube Api Core", coreHealthCheck);
		
		
		environment.jersey().packages("com.wynk.youtube.playlist.finder.provider");
		environment.jersey().register(productResource);
		environment.jersey().register(cem);
		environment.lifecycle().manage(this);
	}

	@Override
	public String getName() {
		return "youtube-playlist-finder";
	}

	@Override
	public void initialize(final Bootstrap<CoreServiceConfiguration> bootstrap) {
		 bootstrap.addBundle(new SwaggerBundle<CoreServiceConfiguration>() {
	            @Override
	            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(CoreServiceConfiguration sampleConfiguration) {
	            sampleConfiguration.swaggerBundleConfiguration.setTitle("Wynk Youtube Apis");
	            	return sampleConfiguration.swaggerBundleConfiguration;
	           
	            }
	        });
	}

	@Override
	public void start() throws Exception {
	}

	@Override
	public void stop() throws Exception {
		final ComboPooledDataSource comboPooledDataSource = classPathXmlApplicationContext
				.getBean(ComboPooledDataSource.class);
		comboPooledDataSource.close();
		classPathXmlApplicationContext.close();
	}
}