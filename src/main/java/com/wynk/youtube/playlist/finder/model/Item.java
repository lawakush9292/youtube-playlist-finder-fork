package com.wynk.youtube.playlist.finder.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Item {

	private Snippet snippet;
	private ContentDetail contentDetail;
	private final String streamUrl;

	@SuppressWarnings("unused")
	private Item() {
		this(null, null, null);
	}

	public Item(final Snippet snippet, final ContentDetail contentDetail, final String streamUrl) {
		this.snippet = snippet;
		this.contentDetail = contentDetail;
		this.streamUrl = streamUrl;
	}

	@JsonProperty("snippet")
	public Snippet getSnippet() {
		return snippet;
	}

	@JsonProperty("contentDetails")
	public ContentDetail getContentDetail() {
		return contentDetail;
	}

	public String getStreamUrl() {
		return streamUrl;
	}

}
