package com.wynk.youtube.playlist.finder.http;

import com.wynk.youtube.playlist.finder.model.PlayListPayLoad;

public interface WynkHttpRequester {

	void createWynkEvent( PlayListPayLoad playListPayLoad);

}
