package com.wynk.youtube.playlist.finder.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class PlayList {

	private final String id;
	private final List<Item> items;

	@SuppressWarnings("unused")
	private PlayList() {
		this(null, null);
	}

	public PlayList(final String id, final List<Item> items) {
		this.id = id;
		this.items = items;
	}

	@JsonProperty("playlistId")
	public String getId() {
		return id;
	}

	@JsonProperty("items")
	public List<Item> getItems() {
		return items;
	}

}
