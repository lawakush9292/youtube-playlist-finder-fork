package com.wynk.youtube.playlist.finder.service;

import java.util.Optional;

import com.wynk.youtube.playlist.finder.model.PlayList;

public interface PlayListService {

	Optional<PlayList> getPlayListItems(String playList);

}
