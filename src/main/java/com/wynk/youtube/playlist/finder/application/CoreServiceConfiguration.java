package com.wynk.youtube.playlist.finder.application;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;


public class CoreServiceConfiguration extends Configuration {
	@JsonProperty("swagger")
    public SwaggerBundleConfiguration swaggerBundleConfiguration;
}
