package com.wynk.youtube.playlist.finder.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wynk.youtube.playlist.finder.http.WynkHttpRequester;
import com.wynk.youtube.playlist.finder.http.YoutubeApiRequester;
import com.wynk.youtube.playlist.finder.model.PlayList;
import com.wynk.youtube.playlist.finder.model.PlayListPayLoad;
import com.wynk.youtube.playlist.finder.service.PlayListService;
import com.wynk.youtube.playlist.finder.util.Constants;

@Service
public class PlayListServiceImpl implements PlayListService {

	private final YoutubeApiRequester youtubeApiRequester;

	private final WynkHttpRequester wynkHttpRequester;

	@Autowired
	public PlayListServiceImpl(final YoutubeApiRequester youtubeApiRequester,
			final WynkHttpRequester wynkHttpRequester) {
		this.youtubeApiRequester = youtubeApiRequester;
		this.wynkHttpRequester = wynkHttpRequester;
	}

	@Override
	public Optional<PlayList> getPlayListItems(final String playListId) {
		final Optional<PlayList> playListItems = youtubeApiRequester.findPlayListItems(playListId);
		if (playListItems.isPresent()) {
			playListItems.get().getItems().stream().forEach(item -> {
				wynkHttpRequester.createWynkEvent(PlayListPayLoad.createWithItems(item,
						createStreamURL(item.getContentDetail().getVideoId(), playListId)));
			});
		}
		return playListItems;
	}

	private String createStreamURL(final String videoId, final String playListId) {
		return new StringBuilder().append(Constants.WATCH_URL).append(videoId).append(Constants.LIST).append(playListId)
				.toString();
	}
}
